import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AddTransaction from "../components/addTransaction/addTransaction";
import listofAllTransaction from '../components/listOfTransaction/listOfTransaction'
import listofAllUserAccounts from '../components/listofAllUserAccounts/listofAllUserAccounts'
import Home from '../components/Home/home'
import Signin from '../components/signin/signin';
import Signup from '../components/signup/signup';
import AddUser from "../components/addUserToAccount/addUserToAccount"
import PrivateRoute from "../auth/helper/PrivateRoutes";
const Routes = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/signup" exact component={Signup}/>
        <Route path="/signin" exact component={Signin}/>
        {/* <Route path="/add-transaction" exact component={AddTransaction}/>
        <Route path="/get-transactionDetails" exact component={listofAllTransaction}/>
        <Route path="/listofaccount" exact component={listofAllUserAccounts}/>
        <Route path="/addUser" exact component={AddUser}/> */}
        <PrivateRoute path="/add-transaction" exact component={AddTransaction}></PrivateRoute>
        <PrivateRoute path="/get-transactionDetails" exact component={listofAllTransaction}></PrivateRoute>
        <PrivateRoute path="/listofaccount" exact component={listofAllUserAccounts}></PrivateRoute>
        <PrivateRoute path="/addUser" exact component={AddUser}></PrivateRoute>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
