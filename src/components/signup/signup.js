import React, { Fragment ,useState} from "react";
import { Form, FormGroup, Label, Input, FormFeedback, FormText, Container,Button } from 'reactstrap';
import Base from '../Base/base';
import {signUp} from '../../auth/helper/signupsignin';

const Signup = ()=>{
    const [values,setValues] = useState({
        name:"",
        email:"",
        password:"",
        confirmPassword:"",
        error:"",
        success:false
    });

    const {name,email,password,confirmPassword,error,success} = values;

   const changeHandler = name => event =>{
        setValues({...values,error:false,[name]:event.target.value})
    }

    const onSubmit = (event)=>{
        event.preventDefault();
        setValues({...values,error:false});
        //console.log(values)
        if(password.length>=8 && confirmPassword.length>=8){
            signUp({name,email,password,confirmPassword}).then(data=>{
                if(data.error){
                    setValues({ ...values,error:data.error, success: false });
                }else{
                    
                   setValues({...values,name:'safgads',email:'',password:'',confirmPassword:'',error:'',success:true})
                    //console.log(JSON.parse(data))
                     
                }
                
            })
            .catch((err)=>{
                console.log(err)
            })
        }
        else{
            setValues({...values,error:"Password must be at least 8 character",success:false})
        }
      
    }

    const signUpForm = ()=>{
        return(
            <Container>
                <Fragment>
                <Form>
                    <FormGroup>
                        <Label for="name">Email</Label>
                        <Input type="text" name="name" id="name" value={name} placeholder="Enter your name" onChange={changeHandler("name")} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="email">Email</Label>
                        <Input type="email" name="email" id="email" value={email} placeholder="Enter your email" onChange={changeHandler("email")} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Password</Label>
                        <Input type="password" name="password" id="password" value={password} placeholder="Enter password" onChange={changeHandler("password")} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="confirmPassword">Password</Label>
                        <Input type="password" name="confirmPassword" id="confirmPassword" value={confirmPassword} placeholder="Enter password to confirm" onChange={changeHandler("confirmPassword")}/>
                    </FormGroup>
                    <Button color="success" onClick={onSubmit} className="btn btn-block">Submit</Button>
                </Form>
            </Fragment>
            </Container>
        )
    }

   return(
      <div>
          <Base title="Signup Page" description="Please do Registration here">
          {signUpForm()}
          <p className="text-white text-center">{JSON.stringify(values)}</p>
          </Base>
      </div>
   )
}

export default Signup;
