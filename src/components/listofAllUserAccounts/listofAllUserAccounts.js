import React, { Component, Fragment } from 'react'
import Base from '../Base/base';
import { Row, Col, Table, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { accountDetails, addAccount, editAccount ,deleteAccount} from '../../auth/helper/dashboardHelper';

let userId = '';
class listofAllUserAccounts extends Component {
    acctName = [];
    acctBalance = [];
    constructor(props) {
        super();
        this.state = {
            accountDetails: [
                {
                    name: "",
                    balance: "",
                    id: ""
                }
            ],
            isModalOpen: false,
            modalData: [
                {
                    input: "",
                    id: '',
                    header: '',
                    type: ''
                }],
        }
    }
    componentDidMount() {
        const userdata = localStorage.getItem('userDetails');
        const udata = JSON.parse(userdata);
         userId = udata._id
        // console.log(userId)
        accountDetails({ userId }).then(({ data }) => {
           // console.log(data)
            this.setState({
                accountDetails: data
            })
        })

    }
    toggleModal = (type, id = null, name = null) => {
        if (type === "new") {
            this.setState({
                modalData: {
                    type: "new",
                    id: id,
                    header: "Add New Account",
                    input: name,
                }

            })
        }
        else if (type === "edit") {
            this.setState({
                modalData: {
                    type: "edit",
                    id: id,
                    header: "Edit Account Name",
                    input: name
                }

            })
        }
        this.setState({ isModalOpen: !this.state.isModalOpen });
    }
    saveData = () => {
        console.log(this.state.modalData)
         if(this.state.modalData.type === "new"){
             
             const obj = {
                 userId:userId,
                 name:this.state.modalData.input
             }
             addAccount({obj}).then((data)=>{
                 console.log(data);
             }).catch((err)=>{
                 console.log(err);
             })
         }
         else if(this.state.modalData.type === "edit"){
             var obj = {
                 id:this.state.modalData.id,
                 name:this.state.modalData.input
             }
           
            editAccount({obj}).then(({data})=>{
                //console.log(data);
                window.location.reload();
            })
         }
    }
    renderTableData() {
        //console.log(this.state.accountDetails)
        return (this.state.accountDetails.map((data, index) => {
            //console.log(data)
            // const {name,balance } = data //destructuring
            return (
                <tr key={index}>
                    <td>{index}</td>
                    <td>{data.name}</td>
                    
                    <td>
                        <Button color="info" onClick={() => { this.toggleModal("edit", data._id, data.name); }}>Edit</Button>&nbsp;&nbsp;
                        <Button color="danger" onClick={() => {
                                deleteAccount({id:data._id});
                                }}>
                                    Delete
                        </Button>
                    </td>
                </tr>
            )
        })
        )
    }

    addTransactions = () => {
        console.log("clicked")
        if (localStorage.getItem('token')) {
           // console.log("if")
            return (
                this.props.history.push('/add-transaction')
            )
        }
        else {
            return (
                console.log('Not Authorized User')
            )
        }
    }

    addUser = ()=>{
        console.log("hi")
        return(
            this.props.history.push("/addUser")
        )
    }

    render() {

        return (
            <>
                <Base title="List Of User Accounts">
                    <Row>
                        <Col>
                        <Button onClick={() => { this.toggleModal("new"); }} className="float-right mb-4" size="lg" outline color="success">Add new Account</Button>
                        <Button onClick={ this.addUser} className="float-right mb-4 mr-2" size="lg" outline color="info">Add User to Account</Button>
                            <Table dark>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Account Name</th>
                                       
                                        <th>Operations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderTableData()}
                                </tbody>
                            </Table>
                            <br></br>
                            <Button onClick={this.addTransactions} outline color="info" size="lg">Continue Transactions</Button>
                            {/* <Link className="btn btn-secondary" to="/dashboard">
                              Add Transaction
                        </Link> */}
                        </Col>
                    </Row>
                    <Modal centered isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>{this.state.modalData.header}</ModalHeader>
                        <ModalBody>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Account Name"
                                value={this.state.modalData.input}
                                onChange={(e) => {
                                    this.setState({ modalData:{...this.state.modalData, input: e.target.value} });
                                }}
                            />
                        </ModalBody>
                        <ModalFooter>
                            <Button
                                color="primary"
                                onClick={() => {
                                    this.toggleModal(null);
                                    this.saveData();
                                }}
                            >
                                Save
                            </Button>{" "}
                            <Button color="secondary" onClick={this.toggleModal}>
                                Cancel
                            </Button>
                        </ModalFooter>
                    </Modal>
                </Base>
            </>
        )
    }
}

export default listofAllUserAccounts;