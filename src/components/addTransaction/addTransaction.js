import React,{Component, useState} from 'react';

import "../../../src/styles.css";
import Base from '../Base/base';

import { Container,Row,Col,Button, Form, FormGroup, Label, Input,Modal,ModalBody,ModalFooter,Jumbotron,ModalHeader } from 'reactstrap';

import {accountDetails,categoryDetails,transactionDetails,transferMoney} from '../../auth/helper/dashboardHelper';

var aitemsArr = [];
var citemsArr = [];
var accountBalance = [];

class Dashboard extends Component{
    
    constructor(){
        super();
        this.state = {
            accountData:[' '],
            accountName:[' '],
           categoryName:[' '],
           toggler:0,
           userId:'',
           obj:{
               transactionType:'',
               accountName:'',
               categoryName:'',
               amount:''
           },
           isModalOpen:false,
           accountDetails:'',
           modalData:[
               {
                   from:'',
                   to:'',
                   accountName:'',
                   amount:''
               }
           ],
           userDetails:{
               name:'',
               email:''
           }
        }
        
    }
    
    componentDidMount(){
        const userdata = localStorage.getItem('userDetails');
        const udata = JSON.parse(userdata);

        const userID=udata._id;
        const userId= userID;
        this.setState({userId:userID});
        this.setState({userDetails:{name:udata.name,email:udata.email}})
        
        accountDetails({userId}).then(({data})=>{
               //console.log(data)
            this.setState({accountDetails:data})
                for(let i=0;i<data.length;i++){
                   
                   aitemsArr.push(data[i].name);
                   accountBalance.push(data[i].balance);
                   
                }
             
               this.setState({accountName:aitemsArr});
               this.setState({accountData:accountBalance});
            
        }).catch((err)=>{
            console.log(err);
        });
        
        categoryDetails({userID}).then(({data})=>{
            //console.log(data);
            for(let i=0;i<data.length;i++){
               // console.log(data[i].name);
                citemsArr.push(data[i].name)
            }
            this.setState({categoryName:citemsArr});
            this.setState({
                obj:{transactionType:'Expanse',accountName:this.state.accountName[0],categoryName:this.state.categoryName[0],amount:this.state.amount}
            })
        }).catch((err)=>{
            console.log(err)
        })

        // findAccountDetails Based on AccountType
        
    }
     
     onChangeHandler = (e)=>{
       
      
       // this.setState({obj:e.target.value});
       this.state.obj[e.target.name]=e.target.value;
        this.setState({})
       // console.log(this.state.obj)
        
    }
    handleSubmit = (e)=>{
        e.preventDefault();
        //console.log("hi")
        //console.log(this.state.obj);
        var obj = {
            userId:this.state.userId,
            transactionType:this.state.obj.transactionType,
            accounName:this.state.obj.accountName,
            categoryName:this.state.obj.categoryName,
            amount:this.state.obj.amount,
            
        }
        transactionDetails(obj).then((data)=>{
            console.log(data)
        })

    }

    toggleModal = ()=>{
        this.setState({ isModalOpen: !this.state.isModalOpen });
    }

    saveData=()=>{
        let obj = {
            accountId:'',
            accountName:this.state.modalData.accountName,
            to:this.state.modalData.to,
            from:this.state.userDetails.email,
            amount:this.state.modalData.amount,
            userId:this.state.userId
        }
        for(let dt of this.state.accountDetails){
            if(dt.name === obj.accountName){
                obj.accountId=dt._id;
               
            }
        }

        transferMoney(obj)
    }
    
    render(){
        return(
       
            <Base title="Add Transactions">
                <hr></hr>
                
                <Row>                    
                   <Col>
                   <Button outline color="info" size="lg" className="float-left mb-2 mt-0" onClick={()=>{
                       return(
                           
                           this.props.history.push('/get-transactionDetails')
                       )
                   }}>Transaction Details</Button>
                   <Button outline color="info" size="lg" className="float-right mb-2 mt-0" onClick={this.toggleModal}>Transfer Money</Button>
                   <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                   <label className="mt-5">Transaction Type</label>
                                    <Input type="select" name="transactionType" id="transactionType" value={this.state.obj.transactionType} onChange={this.onChangeHandler}>
                                        <option value="Expanse">Expanse</option>
                                        <option value="Income">Income</option>
                                        
                                    </Input>
                                  
                                    <Label for="exampleSelect">Select Account Name</Label>
                                    <Input type="select" name="accountName" id="accountName" value={this.state.obj.accountName} onChange={this.onChangeHandler}>
                                        {this.state.accountName.map((elem,i)=>{
                                           // console.log(i);
                                           return <option key={i} value={elem}>{elem}</option>
                                            
                                        })}
                                    </Input>
                                
                                    <Label for="exampleSelect">Category</Label>
                                    <Input type="select" name="categoryName" id="categoryName" value={this.state.obj.categoryName} onChange={this.onChangeHandler}>
                                        {this.state.categoryName.map((elem,i)=>{
                                           // console.log(i);
                                           return <option key={i} value={elem}>{elem}</option>
                                            
                                        })}
                                    </Input>
                                   
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleAmount">Amount</Label>
                                <Input type="number" name="amount" id="amount" value={this.state.obj.amount} placeholder="Enter Amount" onChange={this.onChangeHandler}/>
                            </FormGroup>
                            {/* <Button outline color="primary" size="lg" block >Save</Button> */}
                            <Input type="submit" value="Submit"></Input>
                       </Form>
                   </Col>
                   
                </Row>
                
                    <Modal centered isOpen={this.state.isModalOpen} toggle={this.toggleModal} size="lg">
                    <Jumbotron className="p-3">
                            <ModalHeader toggle={this.toggleModal}>Transfer Money</ModalHeader>

                            <ModalBody>
                                    <h4>Account Holder Name :<span>{this.state.userDetails.name}</span> </h4>
                                          <FormGroup>
                                               <Label>Account Name:</Label> 
                                                          <Input type="select" name="accountName" id="accountName" value={this.state.modalData.accountName || ''} onChange={(e)=>{
                                                              
                                                                   this.setState({ modalData:{...this.state.modalData, accountName: e.target.value} });
                                                                   
                                                             }}>
                                                             {this.state.accountName.map((elem,i)=>{
                                                                    //console.log(elem);
                                                             return <option key={i} value={elem}>{elem}</option>
                                                                        
                                                              })}
                                                           </Input>      
                                                    </FormGroup>
                                                
                                                            <FormGroup>
                                                                <Label>From:</Label>
                                                               <Input type="email" value={this.state.userDetails.email} disabled></Input>
                                                                
                                                            </FormGroup>
                                                            <FormGroup>
                                                                <Label>To:</Label> 
                                                              <Input type="email" value={this.state.modalData.to || ''} placeholder="Enter email of addedUser  in   
                                                                   your Account "
                                                                    onChange={(e)=>{
                                                                        e.preventDefault();
                                                                        this.setState({ modalData:{...this.state.modalData, to: e.target.value} });
                                                                    }}
                                                               />
                                                            
                                                            </FormGroup>
                                                            <FormGroup>
                                                                <Label>Amount</Label>
                                                                <Input
                                                                    type="text"
                                                                    value={this.state.modalData.amount || ''}
                                                                    placeholder="Enter Amount"
                                                                    onChange={(e)=>{this.setState({modalData:{...this.state.modalData,amount:e.target.value}})}}
                                                                 />
                                                                
                                                               
                                                            </FormGroup>
                            </ModalBody>
                            <ModalFooter>
                                <Button
                                    color="primary"
                                    onClick={() => {
                                        this.toggleModal(null);
                                        this.saveData();
                                    }}
                                >
                                    Save
                                </Button>{" "}
                                <Button color="secondary" onClick={this.toggleModal}>
                                    Cancel
                                </Button>
                            </ModalFooter>
                            </Jumbotron>
                        </Modal>
                    
            </Base>
       
    )
    }
}

export default Dashboard;