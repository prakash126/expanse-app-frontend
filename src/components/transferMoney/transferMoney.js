import React, { useState } from 'react';
import { FormGroup,Container,Row,Col, Label } from 'reactstrap';

const TranserMoney=()=> {
    const [transaction,setTransaction] = useState(" ");
    const [currentAccountForTransaction,setCurrentAccountForTransaction] = useState(" ");
    const [fromAccountForTransaction,setFromAccountForTransaction] = useState(" ");
  return (
    <>
         <Base title="Transaction Money to Other User">
                <Container fluid className="mt-5 text-dark">
                    <Row>
                        <Col md="6">
                            <Jumbotron className="p-3">
                                <h2>Send Amount</h2>
                                <hr className="my-1"></hr>
                               
                                  <h4>Account Holder Name :<span></span> </h4>
                                  <FormGroup>
                                      <Label>From:</Label>
                                      <select className="form-control" onChange={(e)=>{
                                        setCurrentAccountForTransaction(e.target.value);
                                       }}>
                                         <option>CASH</option>
                                       </select>
                                      
                                   </FormGroup>
                                   <FormGroup>
                                     <Label>To:</Label>
                                     <select className="form-control" onChange={(e)=>{
                                        setCurrentAccountForTransaction(e.target.value);
                                       }}>
                                         <option>CASH</option>
                                       </select>
                                   </FormGroup>
                            </Jumbotron>
                        </Col>
                        <Col md="6">
                                <Jumbotron className="p-3">
                                    
                                </Jumbotron>
                         </Col>
                    </Row>
                </Container>
            </Base>
    </>
  );
}

export default TranserMoney;