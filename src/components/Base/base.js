import React from 'react';
import { Container } from 'reactstrap';
import Navbar from '../../views/navbar'
const Base = ({
    title="",
    description = "",
    className="bg-dark text-white p-4",
    children
})=> {
  return (
    <div>
        <Navbar />
        <Container fluid={true}>
                <div className="jumbotron bg-dark text-white text-center">
                    <h2 className="display-4">{title}</h2>
                    <p className="lead">{description}</p>
                    <div className={className}>
                      {children}
                    </div>
                </div>
               
                <footer className="bg-dark py-3">
                  <div className="bg-success text-white text-center py-3">
                          <h4>If you got any question feel free to reach out!</h4>
                          <button className="btn btn-warning btn-lg">Contact Us</button>
                  </div>
                  <div className="container">
                          <span className="text-muted">
                              An Amazing <span className="text-white">Expanse Manages App</span>&nbsp;Project
                          </span>
                  </div>
        </footer>
        </Container>
    </div>
  );
}

export default Base;