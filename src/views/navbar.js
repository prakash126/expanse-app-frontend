import React,{useState} from 'react';
import { Fragment } from 'react';
import {Link, withRouter } from 'react-router-dom';
import {signOut} from '../auth/helper/signupsignin'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  
 
} from 'reactstrap';

const currentTab = (history,path)=>{
  if(history.location.pathname===path){
      return {color:"#2ecc72"}
  }
  else{
      return {color:"#FFFFFF"}
  }
}

const NavbarComp = ({history})=>{
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return(
    
        <Fragment>
      <Navbar color="primary" light >
        <NavbarBrand  className="text-white"  to="/">My Expanse Managing App</NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} className="mr-2"/>
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar style={{cursor:"pointer"}}>  
                       
            {localStorage.getItem('token')?
                <Fragment>
                    <NavItem>
                          <Link style={currentTab(history,"/")} className="nav-link" to="/listofaccount">
                               UserAccounts
                           </Link>
                    </NavItem>
                    <NavItem>
                            <Link style={currentTab(history,"/")} className="nav-link" to="/add-transaction">
                               AddTransaction
                           </Link>
                    </NavItem>
                    <NavItem>
                            <Link style={currentTab(history,"/")} className="nav-link" to="/get-transactionDetails">
                               TransactionDetails
                           </Link>
                    </NavItem>
                    <NavItem>
                          <Link style={currentTab(history,"/")} className="nav-link" to="/addUser">
                              AddUser
                          </Link>
                    </NavItem>
                    <NavItem>
                        
                          <span className="nav-link text-warning" style={{cursor:"pointer"}} onClick={()=>{
                              signOut(()=>{
                                  history.push("/")
                              })
                          }}>
                              Logout
                          </span>
                     
                    </NavItem>
                </Fragment>
            :<Fragment>
                 <NavItem>
                      <Link style={currentTab(history,"/signin")} className="nav-link" to="/signin">
                            Signin
                      </Link>
                 </NavItem>
                 <NavItem>
                          <Link style={currentTab(history,"/signup")} className="nav-link" to="/signup">
                              Signup
                        </Link>
                       
                 </NavItem>
             </Fragment>}
          </Nav>
        </Collapse>
      </Navbar>
    </Fragment>
    
  );
}

 

export default withRouter(NavbarComp)


// mittal.sarvaiya@jadeglobal.com 

                          