import {API} from '../../backendApi/backendApi';
import axios from 'axios';
import { Redirect } from "react-router-dom";
export  const signUp = user => {
     //console.log("API "+user)
   return axios.post(`${API}auth/signup`,user).then((data)=>{
       return JSON.stringify(data);
    }).catch((err)=>{
        console.log(err);
        return err;
    })
 };

 export const signIn = user=>{
     //console.log("Signin "+user.email)
     return axios.post(`${API}auth/signin`,user).then((data)=>{
         return data
     }).catch((err)=>{
         console.log(err);
         return err;
     })
 }

 export const signOut = next=>{
     //console.log("hi Logout")
     if(typeof window !== "undefined"){
        localStorage.removeItem("token")
        localStorage.removeItem("userDetails")
        next();
        return true;
    }
 }

 export const isAuthenticated = ()=>{
    if(typeof window == "undefined"){
        return false
    }
    if(localStorage.getItem("token")){
        return localStorage.getItem("token");
    }
    else{
        return false;
    }
}