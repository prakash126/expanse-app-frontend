import {API} from '../../backendApi/backendApi';
import axios from 'axios';



export const accountDetails = userId=>{
    //console.log(userId);
    return axios.post(`${API}dashboard/accountDetailBasedOnUserId`,userId,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        return data;
    }).catch((err)=>{
        console.log(err);
        return err;
    })
}



export const categoryDetails = userId=>{
   // console.log(userId);
    return axios.post(`${API}dashboard/categoryDetailBasedOnUserId`,userId,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        return data;
    }).catch((err)=>{
        console.log(err);
        return err;
    })
}

export const transactionDetails = data =>{
    //console.log(data);
    return axios.post(`${API}transaction/transactionpostDetails`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        console.log(data)
        return data;
    }).catch((err)=>{
        console.log(err);
        return err;
    })
}

// CRUD on Account

export const addAccount = data =>{
    console.log(data);
    return axios.post(`${API}accountsOperation/addAccount`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        console.log(data);
        return data;
    }).catch((err)=>{
        console.log(err);
    })
}

export const editAccount = data=>{
    console.log(data);
    return axios.put(`${API}accountsOperation/updateAccount`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        console.log(data);
        return data;
    }).catch((err)=>{
        console.log(err);
    })
}

export const deleteAccount =data=>{
    console.log(data);
    return axios.post(`${API}accountsOperation/deleteAccount`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        console.log(data);
        return data;
    }).catch((err)=>{
        console.log(err);
    })
}

// addUserToAccountByEmail

export const addUserToAccountByEmail = data=>{
    //console.log(data);
    return axios.put(`${API}accountsOperation/addUserToAccountByEmail`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        console.log(data);
        return data;
    }).catch((err)=>{
        console.log(err);
        return err;
    })
}

// getAddedUser

export const getAddedUser = data=>{
    //console.log(data)
    return axios.post(`${API}accountsOperation/getAddedUser`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        console.log(data);
        return data;
    }).catch((err)=>{
        console.log(err);
        return err;
    })

}

// tranfer 

export const transferMoney = data=>{
  //  console.log(data)
  return axios.put(`${API}transaction/transferMoney`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        console.log(data)
        return data;
    }).catch((err)=>{
        console.log(err);
        return err;
    })
}

//CRUD Transaction

export const getTransactionsDetails = data=>{
   // console.log(data);
    return axios.post(`${API}transaction/getTransactionDetails`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
       // console.log(data)
        return data;
    }).catch((err)=>{
        console.log(err);
        return err;
    })
}

export const editTransactionsDetails = data=>{
    //console.log(data);`
    return axios.put(`${API}transaction/editTransaction`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        // console.log(data)
         return data;
     }).catch((err)=>{
         console.log(err);
         return err;
     })
}

export const deleteTransactionsDetails = data=>{
    //console.log(data);
    return axios.post(`${API}transaction/deleteTransaction`,data,{headers:{"Authorization": "Bearer " + localStorage.getItem("token")}}).then((data)=>{
        return data;
    }).catch((err)=>{
        console.log(err);
        return err;
    })
}